import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Api from "../utility/api";

function Posts(props) {
  const [data, setData] = useState([]);
  const [userData, setUserData] = useState({ company: {}, address: {} });
  // console.log(userData);
  useEffect(() => {
    Api.get(`users/${props.match.params.userId}`).then((res) => {
      console.log(res);
      console.log(res.data);
      setUserData(res.data);
    });
  }, []);
  useEffect(() => {
    Api.get(`users/${props.match.params.userId}/posts`).then((res) => {
      console.log(res);
      console.log(res.data);
      setData(res.data);
    });
  }, []);

  const renderItem = (item) => {
    return (
      <div>
        <Link to={`${item.id}`}>
          {item.title}
        </Link>{" "}
      </div>
    );
  };

  const renderItems = () => {
    return data.map((item) => renderItem(item));
  };

  const renderUserData = () => {
    return (
      <>
       <h4> <Link to={`/users/${userData.id}`}>{userData.name}</Link></h4>
        city: {userData.address.city}
        <br></br>
        company: {userData.company.name}
        <br></br>
      </>
    );
  };

  return (
    <div className="posts">
      <h6>some Data about this Author</h6>
      {renderUserData()}
      <hr></hr>
      <h6>Posts List</h6>
      {renderItems()}
    </div>
  );
}

export default Posts;
