import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Api from "../utility/api";

function Nav() {
  return (
    <div className="nav">
     <Link to={`/`}>home</Link> {' '}
     <Link to={`/tags`}>tags</Link>
    </div>
  );
}

export default Nav;
